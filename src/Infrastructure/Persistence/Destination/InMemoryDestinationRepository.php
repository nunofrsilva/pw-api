<?php


namespace App\Infrastructure\Persistence\Destination;


use App\Domain\Destination\Destination;
use App\Domain\Destination\DestinationNotFoundException;
use App\Domain\Destination\DestinationRepository;
use App\Domain\Destination\UserNotFoundException;

class InMemoryDestinationRepository implements DestinationRepository
{

    /**
     * @var Destination[]
     */
    private $destinations;

    private $db;

    /**
     * InMemoryDestinationRepository constructor.
     * @param Destination[] $destinations
     */
    public function __construct()
    {
        $this->db = new \PDO("sqlite:".__DIR__."/destinations.db");
    }

    /**
     * Get All Destinations
     * @param string $orderBy
     * @return array
     */
    public function findAll(string $orderBy = "totalAccess"): array
    {
        $destinations = $this->executeQuery('select * from destinations order by "'.$orderBy.'" desc');
        return array_values($destinations);
    }

    /**
     * Get Destination by ID
     *
     * @param int $id
     * @return Destination
     * @throws DestinationNotFoundException
     */
    public function findUserOfId(int $id): Destination
    {
        $destinations = $this->executeQuery("select * from destinations where id={$id}");
        if (empty($destinations)) {
            throw new DestinationNotFoundException();
        }
        return $destinations[0];
    }

    /**
     * Search destinations by patterns
     * @param string $keyword
     * @return array
     * @throws DestinationNotFoundException
     */
    public function searchByKeyword(string $keyword): array {
        $terms = explode(" ", trim($keyword));
        $regex = "";
        $queryPattern = "";
        foreach ($terms as $term) {
            $regex .= "(?=.*?({$term}))*";
            $queryPattern .="%{$term}";
        }
        $destinations = $this->executeQuery("select * from destinations where content LIKE '{$queryPattern}%'");
        $results = [];
        foreach ($destinations as $destination) {
            $matches = [];
            if (preg_match("/{$regex}/i", $destination->getContent(), $matches)) {
                $matches = array_filter($matches);
                array_push($results, [
                    'destination' => $destination,
                    'totalPatters' => count($matches),
                    'totalAccess' => $destination->getTotalAccess(),
                    'totalAccessByTeenagers' => $destination->getTotalAccessByTeenagers(),
                    'totalAccessByAdults' => $destination->getTotalAccessByAdults(),
                    'totalAccessBySeniors' => $destination->getTotalAccessBySeniors()
                ]);
            }
        }
        if (empty($results)) {
            throw new DestinationNotFoundException();
        }

        return $results;
    }

    /**
     * Update destination
     * @param Destination $destination
     */
    public function updateDestination(Destination $destination): void {
        $stmt = $this->db->prepare("UPDATE destinations SET 
                        title=:title, 
                        content=:content,
                        image=:image,
                        totalAccess=:totalAccess, 
                        totalAccessByTeenagers=:totalAccessByTeenagers, 
                        totalAccessByAdults=:totalAccessByAdults, 
                        totalAccessBySeniors=:totalAccessBySeniors
                        WHERE id=:id");
        $resutl = $stmt->execute([
            ':title' => $destination->getTitle(),
            ':content' => $destination->getContent(),
            ':image' => $destination->getImage(),
            ':totalAccess' => $destination->getTotalAccess(),
            ':totalAccessByTeenagers' => $destination->getTotalAccessByTeenagers(),
            ':totalAccessByAdults' => $destination->getTotalAccessByAdults(),
            ':totalAccessBySeniors' => $destination->getTotalAccessBySeniors(),
            ':id' => $destination->getId()
        ]);
    }

    /**
     * Execute query and return an array of Destinations
     * @param string $query
     * @return array
     */
    private function executeQuery(string $query): array {
        $stmt=$this->db->query($query);
        $results=$stmt->fetchAll();
        $destinations = [];
        foreach ($results as $result) {
            $destination = new Destination(
                $result['id'],
                $result['title'],
                $result['content'],
                $result['totalAccess'],
                $result['totalAccessByTeenagers'],
                $result['totalAccessByAdults'],
                $result['totalAccessBySeniors'],
                $result['image']
            );
            array_push($destinations, $destination);
        }
        return $destinations;
    }

}