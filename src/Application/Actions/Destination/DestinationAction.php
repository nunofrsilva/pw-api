<?php


namespace App\Application\Actions\Destination;


use App\Application\Actions\Action;
use App\Domain\Destination\DestinationRepository;
use Psr\Log\LoggerInterface;

abstract class DestinationAction extends Action
{
    /**
     * @var DestinationRepository
     */
    protected $destinationRepository;

    /**
     * DestinationAction constructor.
     * @param LoggerInterface $logger
     * @param DestinationRepository $destinationRepository
     */
    public function __construct(LoggerInterface $logger, DestinationRepository $destinationRepository)
    {
        parent::__construct($logger);
        $this->destinationRepository = $destinationRepository;
    }


}