<?php


namespace App\Domain\Destination;


use App\Domain\DomainException\DomainRecordNotFoundException;

class DestinationNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The destination you requested does not exist.';
}