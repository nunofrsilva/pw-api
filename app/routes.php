<?php
declare(strict_types=1);

use App\Application\Actions\Destination\ListDestinationsAction;
use App\Application\Actions\Destination\SearchDestinationsAction;
use App\Application\Actions\Destination\ViewDestinationAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->group('/destinations', function (Group $group) {
       $group->get('', ListDestinationsAction::class);
       $group->get('/search', SearchDestinationsAction::class);
       $group->get('/{id}', ViewDestinationAction::class);
    });
};
