<?php


namespace App\Application\Actions\Destination;


use App\Domain\Destination\Destination;
use App\Domain\Destination\DestinationNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;

class SearchDestinationsAction extends DestinationAction
{

    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        if (empty($queryParam) || !isset($queryParam['pattern'])){
            throw new DestinationNotFoundException("Search pattern not fount in your request.");
        }
        $keyword = $queryParam['pattern'];
        $userType = null;
        if(isset($queryParam['userType'])) {
            $userType = $queryParam['userType'];
        }
        $results = $this->destinationRepository->searchByKeyword($keyword);
        $destinations = $this->sort($results, $userType);
        return $this->respondWithData($destinations);
    }

    /**
     * Sort search results
     *
     * @param array $results
     * @param null $userType
     * @return array
     */
    private function sort(array $results, $userType = null):array {

        $totalAccess = array_column($results, 'totalAccess');
        $totalBySearch = array_column($results, 'totalPatters');
        if ($userType !== null && Destination::checkUserType($userType)) {
            $totalByUser = array_column($results, Destination::getUserType($userType));
            array_multisort($totalBySearch,  SORT_DESC, $totalByUser, SORT_DESC, $totalAccess, SORT_DESC, $results);
        } else {
            // Sort the data with volume descending, edition ascending
            // Add $data as the last parameter, to sort by the common key
            array_multisort($totalBySearch, SORT_DESC, $totalAccess, SORT_DESC, $results);
        }
        return $results;
    }

}