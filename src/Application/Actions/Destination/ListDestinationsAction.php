<?php


namespace App\Application\Actions\Destination;


use App\Domain\Destination\Destination;
use Psr\Http\Message\ResponseInterface as Response;

class ListDestinationsAction extends DestinationAction
{

    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        $orderBy = "totalAccess";
        if(isset($queryParam['userType'])) {
            $userType = $queryParam['userType'];
            if (Destination::checkUserType($userType)) {
                $orderBy = Destination::getUserType($userType);
            }
        }
        $destinations = $this->destinationRepository->findAll($orderBy);
        $this->logger->info("Destination list was viewed.");
        return $this->respondWithData($destinations);
    }
}