<?php


namespace App\Domain\Destination;


class Destination implements \JsonSerializable
{

    const USER_TYPES = [
        'teenagers' => 'totalAccessByTeenagers',
        'adult' => 'totalAccessByAdults',
        'senior' => 'totalAccessBySeniors',
    ];

    /**
     * @var int|null
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $image;

    /**
     * @var int
     */
    private $totalAccess;
    /**
     * @var int
     */
    private $totalAccessByTeenagers;
    /**
     * @var int
     */
    private $totalAccessByAdults;
    /**
     * @var int
     */
    private $totalAccessBySeniors;

    /**
     * Destination constructor.
     * @param int|null $id
     * @param string $title
     * @param string $content
     * @param int $totalAccess
     * @param int $totalAccessByTeenagers
     * @param int $totalAccessByAdults
     * @param int $totalAccessBySeniors
     * @param string $image
     */
    public function __construct(?int $id, string $title, string $content, int $totalAccess, int $totalAccessByTeenagers, int $totalAccessByAdults, int $totalAccessBySeniors, string $image)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->totalAccess = $totalAccess;
        $this->totalAccessByTeenagers = $totalAccessByTeenagers;
        $this->totalAccessByAdults = $totalAccessByAdults;
        $this->totalAccessBySeniors = $totalAccessBySeniors;
        $this->image = $image;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getTotalAccess()
    {
        return $this->totalAccess;
    }

    /**
     * @param int $totalAccess
     */
    public function setTotalAccess($totalAccess): void
    {
        $this->totalAccess = $totalAccess;
    }

    /**
     * @return int
     */
    public function getTotalAccessByTeenagers()
    {
        return $this->totalAccessByTeenagers;
    }

    /**
     * @param int $totalAccessByTeenagers
     */
    public function setTotalAccessByTeenagers($totalAccessByTeenagers): void
    {
        $this->totalAccessByTeenagers = $totalAccessByTeenagers;
    }

    /**
     * @return int
     */
    public function getTotalAccessByAdults()
    {
        return $this->totalAccessByAdults;
    }

    /**
     * @param int $totalAccessByAdults
     */
    public function setTotalAccessByAdults($totalAccessByAdults): void
    {
        $this->totalAccessByAdults = $totalAccessByAdults;
    }

    /**
     * @return int
     */
    public function getTotalAccessBySeniors()
    {
        return $this->totalAccessBySeniors;
    }

    /**
     * @param int $totalAccessBySeniors
     */
    public function setTotalAccessBySeniors($totalAccessBySeniors): void
    {
        $this->totalAccessBySeniors = $totalAccessBySeniors;
    }

    /**
     * @param $userType
     * @return int
     */
    public function getTotalAccessBy($userType): int
    {
        $type = self::USER_TYPES[$userType];
        if ($type !== null) {
            return $this->$type;
        }
        return 0;
    }

    public function incrementTotalAccessOf($userType) {
        $type = self::USER_TYPES[$userType];
        if ($type !== null) {
            $this->$type += 1;
        }
    }

    public function incrementTotalAccess() {
        $this->totalAccess += 1;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'image' => $this->image
        ];
    }

    public static function checkUserType($userType)
    {
        return isset(self::USER_TYPES[$userType]);
    }

    public static function getUserType($userType)
    {
        return self::USER_TYPES[$userType];
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }


}