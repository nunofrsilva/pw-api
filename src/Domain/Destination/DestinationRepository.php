<?php


namespace App\Domain\Destination;

interface DestinationRepository
{

    /**
     * @param string $orderBy
     * @return Destination[]
     */
    public function findAll(string $orderBy): array;

    /**
     * @param int $id
     * @return Destination
     * @throws DestinationNotFoundException
     */
    public function findUserOfId(int $id): Destination;

    /**
     * @param string $keyword
     * @return array
     * @throws DestinationNotFoundException
     */
    public function searchByKeyword(string $keyword): array;

    /**
     * Update Destination
     * @param Destination $destination
     */
    public function updateDestination(Destination $destination): void;

}