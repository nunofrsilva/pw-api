<?php
declare(strict_types=1);

use App\Domain\Destination\DestinationRepository;
use App\Domain\User\UserRepository;
use App\Infrastructure\Persistence\Destination\InMemoryDestinationRepository;
use App\Infrastructure\Persistence\User\InMemoryUserRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        UserRepository::class => \DI\autowire(InMemoryUserRepository::class),
        DestinationRepository::class => \DI\autowire(InMemoryDestinationRepository::class),
    ]);
};
