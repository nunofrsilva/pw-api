<?php


namespace App\Application\Actions\Destination;


use App\Domain\Destination\Destination;
use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class ViewDestinationAction extends DestinationAction
{

    protected function action(): Response
    {
        $destinationId = (int) $this->resolveArg('id');
        $destination = $this->destinationRepository->findUserOfId($destinationId);
        $queryParam = $this->request->getQueryParams();
        $userType = null;
        if(isset($queryParam['userType'])) {
            $userType = $queryParam['userType'];
        }
        $destination = $this->updateCounters($destination, $userType);

        return $this->respondWithData($destination);
    }

    private function updateCounters(Destination $destination, $userType): Destination {

        if (Destination::checkUserType($userType)) {
            $destination->incrementTotalAccessOf($userType);
        }
        $destination->incrementTotalAccess();
        // Call Update method
        $this->destinationRepository->updateDestination($destination);
        return $destination;
    }

}